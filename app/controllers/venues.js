"use strict";
var jwt                     = require('jsonwebtoken');
var config                  = require('../../config');
var graph                   = require('fbgraph');
var venuesController        = require('../controllers/venues');
var _                       = require("underscore");

var superSecret = config.secret;

var self = module.exports = {  

    get_events : function( req, res, callback ) {
 

      var options = {
          pool:     { maxSockets:  Infinity }
        , headers:  { connection:  "keep-alive" }
      };

      graph.setAccessToken(config.fb_id+'|'+config.fb_app_secret);

      var target = req.body.target;   //terrabastall,salagarage,innermetalproductions
      var options = req.body.options; //limit=10&....
      var fields = req.body.fields;   //

      var url = "events/?ids="+target+"&fields="+fields+"&"+options;
      //var url = "events/?ids=salagarage,innermetalproductions&limit=5";

      try {

          graph.setOptions(options).get(url, function(err, fbres) {
              callback(fbres);
          });  

      } catch (e) {
      }

    },   
    get_single_event : function( req, res, callback ) {
 

      var options = {
          pool:     { maxSockets:  Infinity }
        , headers:  { connection:  "keep-alive" }
      };

      graph.setAccessToken(config.fb_id+'|'+config.fb_app_secret);

      var fields = req.body.fields; 
      console.log(fields)

      let id = req.params.id;
      var url = '/' + id + '?fields=' + fields;

      try {

          graph.setOptions(options).get(url, function(err, events) {

              if (!events) {
                  res.send({ status: 'error', message: 'Failed to catch events' });
              }else{
                  res.send( 
                    events
                    );
              }
            });

      } catch (e) {
      }

    },   
    try : function( req, res, callback ) {

      console.log('try');
 
      var options = {
          pool:     { maxSockets:  Infinity }
        , headers:  { connection:  "keep-alive" }
      };

      graph.setAccessToken(config.fb_id+'|'+config.fb_app_secret);

      var target = req.body.target;   //terrabastall,salagarage,innermetalproductions
      var options = req.body.options; //limit=10&....
      var fields = req.body.fields;   //

      //var url = "events/?ids="+target+"&fields="+fields+"&"+options;
      var url = "events/?ids=4whatparty,terrabastall,salagarage,innermetalproductions,losubmarinodereus&limit=5&fields=place,name,start_time&debug=all";
      //var url = '/?q=SELECT name,cover,category,description,place,updated_time,start_time,interested_count FROM events WHERE salagarage,innermetalproductions ORDER BY start_time DESC LIMIT 0,10';


 // https://graph.facebook.com/fql?access_token=[TOKEN]&q=
 //    SELECT id, fromid, text, time, likes, user_likes FROM comment
 //      WHERE object_id = [OBJECT_ID] ORDER BY time DESC LIMIT 0,[N]

      try {

          graph.setOptions(options).get(url, function(err, events) {

            // IMPORTANT
            // refactor all events, previous refactored by club, ordering them by date
            // IMPORTANT
      console.log(events);

            var refactored_events_without_club_level = {};
             refactored_events_without_club_level = self.refactorEventsWithoutClubReference( events );


              var refactored_events_ordered_by_date = {};
              // var pushto = [];

      refactored_events_without_club_level.data.sort( function( a, b )
      {
        // Sort by the 2nd value in each array
        if ( a['start_time'] == b['start_time'] ) return 0;
        return a['start_time'] < b['start_time'] ? -1 : 1;
      });
              res.send(refactored_events_without_club_level);
          });  

      } catch (e) {
      }

    },    
    get_facebook_pages_venues : function(req, res, next) {

        try {

          self.get_events( req, res, function(events){
            if (!events) {
                res.send({ status: 'error', message: 'Failed to catch events' });
            }else{
                res.send( 
                  self.refactorEventsOrderByDateASC( 
                     self.refactorEventsWithoutClubReference( events )  
                    ) 
                  );
            }
          });

        } catch (e) {

        }

    }, 
    get_facebook_pages_venues_by_advertiser : function(req, res, next) {

        try {

          self.get_events( req, res, function(events){
            if (!events) {
                res.send({ status: 'error', message: 'Failed to catch events' });
            }else{
                res.send( events );
            }
          });

        } catch (e) {

        }
    },
    refactorEventsWithoutClubReference : function(events){

        var refactoredEvents = {};
        var pushto = [];

        _.each(events, function (item, index) {

            _.each(item['data'], function (sub_item, sub_index) {
                  var data = [];

                 data                =   sub_item;
                 data.index         =   index;

                pushto.push(data);
            });   
        });        
        
        refactoredEvents.data = pushto;

      return refactoredEvents;
    },
    refactorEventsOrderByDateASC : function(events){

      events.data.sort( function( a, b )
      {
        // Sort by the 2nd value in each array
        if ( a['start_time'] == b['start_time'] ) return 0;
        return a['start_time'] < b['start_time'] ? -1 : 1;
      });

      return events;
    }

};