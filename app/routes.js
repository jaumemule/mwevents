
var config     = require('../config');
var jwt        = require('jsonwebtoken');

// super secret for creating tokens
var superSecret = config.secret;

module.exports = function(app, express) {

	var app = app;

	// ---------------------------------------------------------
	// MIDDLEWARES
	// ---------------------------------------------------------

	//var authTokenMiddleware = require('../app/middlewares/authToken');

	// ---------------------------------------------------------
	// CONTROLLERS
	// ---------------------------------------------------------

	//var UserController 	= require('../app/controllers/user');
	var IndexController = require('../app/controllers/index');
	var VenuesController = require('../app/controllers/venues');

	// ---------------------------------------------------------
	// get an instance of the router for api routes
	// ---------------------------------------------------------
	var apiRoutes = express.Router(); 

	// ---------------------------------------------------------
	// authentication (no middleware necessary since this isnt authenticated)
	// ---------------------------------------------------------

	//apiRoutes.post('/authenticate', UserController.authenticate);

	// ---------------------------------------------------------
	// Global middlewares examples (by position in document)
	// ---------------------------------------------------------
	//	app.use( apiRoutes, authTokenMiddleware );
	//	apiRoutes.use(function(req, res, next) {

	// ---------------------------------------------------------
	// authenticated routes
	// ---------------------------------------------------------

	//apiRoutes.get('/', IndexController.construct);
	apiRoutes.post('/venues', VenuesController.get_facebook_pages_venues);
	apiRoutes.post('/venues/:id', VenuesController.get_single_event);
	apiRoutes.get('/venues/try', VenuesController.try);
	apiRoutes.post('/venues/advertiser', VenuesController.get_facebook_pages_venues_by_advertiser);
	//apiRoutes.get('/users',authTokenMiddleware.authenticate, UserController.get_users);
	//apiRoutes.post('/users', UserController.createUser);

	apiRoutes.get('/', function(req, res) {
		res.json(req.decoded);
	});

	return apiRoutes;
};